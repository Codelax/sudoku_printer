from dataclasses import dataclass
from typing import List


@dataclass
class Message:
    row: int
    column: int
    value: int
    uuid: str
    nb_of_msg: int
    path: List[str]


class Sudoku():
    def __init__(self):
        self.cells = [0] * 81
        pass

    def pos_column(letter, column):
        if letter in ["A", "D", "G"]:
            return column
        if letter in ["B", "E", "H"]:
            return column + 3
        if letter in ["C", "F", "I"]:
            return column + 6

    def pos_row(letter, row):
        if letter in ["A", "B", "C"]:
            return row
        if letter in ["D", "E", "F"]:
            return row + 3
        if letter in ["G", "H", "I"]:
            return row + 6

    def pos(letter, row, column):
        return (Sudoku.pos_row(letter, row), Sudoku.pos_column(letter, column))

    def __len__(self):
        return len(self.cells)

    def receive_message(self, msg):
        (row, col) = Sudoku.pos(msg.path[0], msg.row, msg.column)
        self.set(row, col, msg.value)

    def set(self, row, column, value):
        self.cells[column - 1 + ((row - 1) * 9)] = value

    def get(self, row, column):
        return self.cells[column - 1 + ((row - 1) * 9)]

    def __str__(self):
        out = []
        line_sep = "+-------+-------+-------+"
        for line in range(0, 9):
            line = self.cells[line * 9: (line + 1) * 9]
            line = [str(x) for x in line]
            line.insert(3, "|")
            line.insert(7, "|")
            line.insert(11, "|")
            line.insert(0, "|")
            line = " ".join(line)
            out.append(line)

        out.insert(0, line_sep)
        out.insert(4, line_sep)
        out.insert(8, line_sep)
        out.insert(12, line_sep)
        return "\n".join(out)
