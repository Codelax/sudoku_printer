from sudoku import Sudoku, Message


def test_sudoku():
    sudoku = Sudoku()
    assert len(sudoku) == 81


def test_receive_message():
    sudoku = Sudoku()
    msg = Message(
        row=1,
        column=2,
        value=7,
        uuid="uuid",
        nb_of_msg=3,
        path=["A", "B", "C"]
    )
    sudoku.receive_message(msg)
    assert sudoku.get(row=1, column=2) == 7


def test_receive_message_B():
    sudoku = Sudoku()
    msg = Message(
        row=1,
        column=2,
        value=5,
        uuid="uuid",
        nb_of_msg=3,
        path=["B"]
    )
    sudoku.receive_message(msg)
    assert sudoku.get(row=1, column=5) == 5


def test_pos():
    assert Sudoku.pos("A", 1, 2) == (1, 2)
    assert Sudoku.pos("B", 1, 2) == (1, 5)
    assert Sudoku.pos("I", 1, 2) == (7, 8)


def test_sudoku_set():
    sudoku = Sudoku()
    sudoku.cells = [0, 0, 1, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 2, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0]
    assert sudoku.get(row=1, column=3) == 1
    assert sudoku.get(row=2, column=4) == 2


def test_sudoku_render():
    sudoku = Sudoku()
    sudoku.cells = [0, 0, 1, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 2, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0,

                    0, 0, 0, 0, 7, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 6, 0,
                    0, 0, 0, 0, 9, 0, 0, 0, 0,

                    0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 4, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 8, 0, 0]
    expected = """
+-------+-------+-------+
| 0 0 1 | 0 0 0 | 0 0 0 |
| 0 0 0 | 2 0 0 | 0 0 0 |
| 0 0 0 | 0 0 0 | 0 0 0 |
+-------+-------+-------+
| 0 0 0 | 0 7 0 | 0 0 0 |
| 0 0 0 | 0 0 0 | 0 6 0 |
| 0 0 0 | 0 9 0 | 0 0 0 |
+-------+-------+-------+
| 0 0 0 | 0 0 0 | 0 0 0 |
| 0 4 0 | 0 0 0 | 0 0 0 |
| 0 0 0 | 0 0 0 | 8 0 0 |
+-------+-------+-------+
"""
    expected = expected.lstrip().rstrip()
    assert expected == str(sudoku)


if __name__ == "__main__":
    test_sudoku_render()
